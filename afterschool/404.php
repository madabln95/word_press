<?php get_header(); ?>

		<div id="content" class="col-xs-12">

			<div id="primary" class="col-xs-12 col-sm-9">

				<h1 class="archive_title">404 Not Found</h1>

				<h2>Oops! We couldn't find that</h2>

				<h3>Try searching for something else</h3>

				<div class="well">
						<?php get_search_form(); ?>
				</div>
			</div><!-- /primary -->

			<?php get_sidebar(); ?>

		</div><!-- /content -->

	<?php get_footer(); ?>