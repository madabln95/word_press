<?php 

// Template Name: Home Page

get_header(); ?>

		<div id="content" class="col-xs-12">
		

			<div id="primary" class="col-xs-12">
			
				<div id="featured" class="col-xs-12">
				
					<img src="<?=get_template_directory_uri()?>/images/after_school.jpg" alt="After School" />
				
					<div class="caption col-xs-12">
						<a href="#">Find out about our programs for children</a>
					</div><!-- /caption -->
				
				</div><!-- /featured -->
				
			</div><!-- /primary -->

		<?php get_sidebar('home') ?>

		</div><!-- /content -->

<?php get_footer(); ?>
		