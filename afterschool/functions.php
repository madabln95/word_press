<?php 

// Register navigation menus 

register_nav_menus();

if(!function_exists('load_my_scripts'))
{
	function load_my_scripts()
	{
		wp_enqueue_style('bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css');
		wp_enqueue_style('afterschool',get_stylesheet_uri(), ['bootstrap']);

		wp_enqueue_script('jquery','https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js',[],null,true);

		wp_enqueue_script('boostrap','https//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js',['jquery'],[],null,true);
	}

	add_action('wp_enqueue_scripts','load_my_scripts',10);

	// To add support for featured images
	add_theme_support('post-thumbnails');

	if(!function_exists('afterschool_get_util_menu'))
	{
		function afterschool_util_menu()
		{
			wp_nav_menu(['menu' => 'util','container' => 'false']);
		}
	}

	if(!function_exists('afterschool_main_menu'))
	{
		function afterschool_main_menu()
		{
			wp_nav_menu(['menu' => 'main','container' => 'false']);	
		}
	}

	if(!function_exists('afterschool_footer_menu'))
	{
		function afterschool_footer_menu()
		{
			wp_nav_menu(['menu' => 'footer','container' => 'false']);	
		}
	}

}