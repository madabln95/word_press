	<footer class="main col-xs-12">

			<nav id="footer">

				<ul class="menu">
					<?php afterschool_footer_menu() ?>
				</ul>

			</nav>

				<p class="copyright">Copyright &copy; <?= date('Y') ?> by After School</p>
		</footer>
		<?php wp_footer(); ?>
		</div><!-- /container -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

	<script src="https//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	<script>

	    $(document).ready(function(){
	    	setMenu();
	    	$(window).resize(function(){
	    		setMenu();
	    	});
	    });

	    function setMenu() {
	    	if($(window).width() < 768) {
	    		$('nav#main').hide();
	    	} else {
	    		$('nav#main').show();
	    	}
	    }

		$('a.menu_toggle').click(function(){
			$('nav#main').toggle();
		});

	</script>

	</body>
</html>