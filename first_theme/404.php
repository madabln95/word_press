<?php get_header() ?>
<body>
	<div id="container">
		
		<div id="wrapper">

		<h1 style="color: #4183d6;padding: 5px;margin: 0px;background: #ccc;
		min-height: 50px"><?php bloginfo(); ?></h1>

		<div id="nav">
				<h3>Menu</h3>
					
					<?php wp_nav_menu(); ?>

				<h3>Archive</h3>
				<ul>
					<?php wp_get_archives(); ?>
				</ul>
		</div>
		<div id="content">

					<article>
							<h1>404 - Not Found</h1>	

							<p>We couldn't find the page you were looking for.</p>
					</article>

		</div>
	
	<?php get_footer() ?>