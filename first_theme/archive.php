<?php get_header() ?>
<body>
	<div id="container">
		
		<div id="wrapper">

		<h1 style="color: #4183d6;padding: 5px;margin: 0px;background: #ccc;
		min-height: 50px"><?php bloginfo(); ?></h1>

		<div id="nav">
				<h3>Menu</h3>
						<?php wp_nav_menu(); ?>

				<h3>Archive</h3>
				<ul>
					<?php wp_get_archives(); ?>
				</ul>
		</div>
		<div id="content">

				<h1><?php echo get_the_archive_title() ?></h1>

			<?php while(have_posts()) : ?>
					<?php the_post(); ?>

				<article>
				
							<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
				
							<p><small>Posted on <?php echo get_the_date()?>, by <?php the_author(); ?>.</small></p>
				
							<p>Posted on <?php the_category(', '); ?></p>

							<?php the_excerpt(); ?>
						
				</article>

			<?php endwhile; ?>
		</div>
	
	<?php get_footer() ?>