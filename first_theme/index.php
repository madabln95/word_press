<?php get_header() ?>
<body>
	<div id="container">
		
		<div id="wrapper">

		<h1 style="color: #4183d6;padding: 5px;margin: 0px;background: #ccc;
		min-height: 50px"><?php bloginfo(); ?></h1>

		<div id="nav">
				<h3>Menu</h3>
					<?php wp_nav_menu(); ?>
				<h3>Archive</h3>
				<ul>
					<?php wp_get_archives(); ?>
				</ul>
		</div>
		<div id="content">

			<?php while(have_posts()) : ?>
					<?php the_post(); ?>

					<article>
						<?php if(is_page() || is_single()) : ?>
							<h1><?php the_title(); ?></h1>
						<?php else: ?>
							<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
						<?php endif; ?>
						

						<?php if(!is_page()) : ?>
							<p><small>Posted on <?php echo get_the_date()?>, by <?php the_author(); ?>.</small></p>
							<p>Posted on <?php the_category(', '); ?></p>
						<?php endif; ?>
	
						<?php if(is_page() || is_single()) : ?>
							<?php the_content(); ?>
						<?php else: ?>
							<?php the_excerpt(); ?>
						<?php endif; ?>

					</article>

				<?php endwhile; ?>
		</div>
	
	<?php get_footer() ?>