<?php get_header() ?>
<body>
	<div id="container">
	<div id="header">
		<h1><?php bloginfo('name'); ?></h1>	
	</div>
	<?php get_sidebar() ?>
	<div id="wrapper">	
		
		<div id="content">

			<?php while(have_posts()) : ?>
					<?php the_post(); ?>

					<article>
						<?php if(is_page() || is_single()) : ?>
							<h1><?php the_title(); ?></h1>
						<?php else: ?>
							<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
						<?php endif; ?>
						

						<?php if(!is_page()) : ?>
							<p><small>Posted on <?php echo get_the_date()?>, by <?php the_author(); ?>.</small></p>
							<p>Posted on <?php the_category(', '); ?></p>
						<?php endif; ?>
	
						<?php if(is_page() || is_single()) : ?>
							<?php the_content(); ?>
						<?php else: ?>
							<?php the_excerpt(); ?>
						<?php endif; ?>

					</article>

				<?php endwhile; ?>
						
				<?php echo paginate_links(); ?>
			<div style="margin-bottom: 130px">
				
			</div>
			</div>
		
	</div>

	
<?php get_footer() ?>