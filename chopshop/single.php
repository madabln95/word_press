<?php get_header() ?>
<body>
	<div id="container">
	<div id="header">
		<h1><?php bloginfo('name'); ?></h1>	
	</div>
	<?php get_sidebar() ?>
	<div id="wrapper">	
		
		<div id="content">

			<?php while(have_posts()) : ?>
					<?php the_post(); ?>

					<article>
							<h1><?php the_title(); ?></h1>

							<p><small>Posted on <?php echo get_the_date()?>, by <?php the_author(); ?>.</small></p>

							<p>Posted on <?php the_category(', '); ?></p>

							<?php the_content(); ?>
						
						
					</article>
				<?php endwhile; ?>
				
			<?php comment_form(); ?>
			<?php 
			$comments = get_comments(array (
				'status' => 'approve',
				'order' => 'ASC',
				'post_id' => $post->ID) );

			 ?>

			 <?php 	if(count($comments)) : 

			 	$count = count($comments);
			 ?>
			 <h4>Total <?=$count?> Comments</h4>
			 <ol>
			 	<?php foreach($comments as $comment) : ?>
			 	<li>
			 		<strong><?php echo $comment->comment_author; ?></strong>
			 		<br>
			 		<em><?php echo $comment->comment_data; ?></em>
			 		<br>
			 		<span><?php echo $comment->comment_context; ?></span>
			 	</li>
			 	<?php endforeach; ?>
			 </ol>
			<?php endif; ?>
			  	<br><br>	<br><br><br>
		</div>
		
	</div>

	
<?php get_footer() ?>