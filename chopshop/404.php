<?php get_header() ?>
<body>
	<div id="container">
	<div id="header">
		<h1><?php bloginfo('name'); ?></h1>	
	</div>
	<?php get_sidebar() ?>
	<div id="wrapper">	
		
		<div id="content">
			<article>
				<h1>404 - Not Found</h1>	
				<p>We couldn't find the page you were looking for.</p>
			</article>
		</div>
		
	</div>

	
<?php get_footer() ?>