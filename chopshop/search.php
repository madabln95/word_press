<?php

get_header()

?>
  

<?php get_header() ?>
<body>
  <div id="container">
  <div id="header">
    <h1><?php bloginfo('name'); ?></h1> 
  </div>
  <?php get_sidebar() ?>
  <div id="wrapper">  
    
    <div id="content">
        
         <?php while(have_posts()) : //start of The Loop ?>
            <?php the_post(); ?>

            <article>
              <h1>You Searched For: <?php echo get_search_query() ?></h1>
              <h2><?php the_title(); ?></h2>
              <?php the_content(); ?>
              <?php the_excerpt(); ?>
            </article>
            <br><br><br><br><br>
            <?php endwhile; ?>
             
            <?=paginate_links() ?>

    </div>
    
  </div>
  
<?php get_footer() ?>