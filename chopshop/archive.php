<?php get_header() ?>
<body>
	<div id="container">
	<div id="header">
		<h1><?php bloginfo('name'); ?></h1>	
	</div>
	<?php get_sidebar() ?>
	<div id="wrapper">	
		
		<div id="content">

			<h1><?php echo get_the_archive_title() ?></h1>
			<?php while(have_posts()) : ?>
					<?php the_post(); ?>
					<article>
							<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
							<p><small>Posted on <?php echo get_the_date()?>, by <?php the_author(); ?>.</small></p>
							<p>Posted on <?php the_category(', '); ?></p>
							<?php the_excerpt(); ?>
					</article>

				<?php endwhile; ?>

						<br><br><br><br><br>
				<?=paginate_links()?>
		</div>
		
	</div>

	
<?php get_footer() ?>